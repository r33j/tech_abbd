import Vue from 'vue'

export const state = () => ({
  cart: [],
  amountCart: 0
})

export const actions = {
  addProduct (context, item) {
    context.commit('add', item)
  }
}

// @ts-ignore
export const mutations = {
  add (state, item) {
    const x = state.cart.find(e => e.item.title === item.title)
    if (x) {
      Vue.set(x.item, 'qty', x.item.qty + 1)
      state.amountCart = state.cart.reduce((a, b) => +a + +b.item.qty, 0)
      this.app.$toast.info(item.title + 'successfully added to cart')
    } else {
      item.qty = 1
      state.cart.push({
        item
      })
      state.amountCart = state.cart.reduce((a, b) => +a + +b.item.qty, 0)
      this.app.$toast.info(item.title + 'successfully added to cart')
    }
  },
  remove (state, item) {
    const x = state.cart.find(e => e.item.title === item.title)
    if (x.item.qty <= 1) {
      this.app.$toast.info(x.item.title + 'successfully deleted from cart')
      state.cart.splice(state.cart.indexOf(x), 1)
      state.amountCart = state.cart.reduce((a, b) => +a + +b.item.qty, 0)
    } else if (x.item.qty > 1) {
      this.app.$toast.info(x.item.title + 'successfully updated from cart')
      Vue.set(x.item, 'qty', x.item.qty - 1)
      state.amountCart = state.cart.reduce((a, b) => +a + +b.item.qty, 0)
    }
  }
}

export const getters = {
  getCart: (state) => {
    return state.cart
  },
  amountCart: (state) => {
    return state.amountCart
  }
}
